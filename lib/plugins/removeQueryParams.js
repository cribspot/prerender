var url = require("url");

module.exports = {
	init: function() {
		this.QUERY_PARAMS = (process.env.QUERY_PARAMS && process.env.QUERY_PARAMS.split(',')) || [];
	},
  beforePhantomRequest: function(req, res, next) {
    var parts = url.parse(req.prerender.url, true),
        param;

    if (parts.query) {
      // Clear out the query params in search
      delete parts.search;

      for(var i = 0; i < this.QUERY_PARAMS.length; i++) {
        param = this.QUERY_PARAMS[i];
        if (parts.query[param]) {
          delete parts.query[param];
        }
      }
    }
    req.prerender.url = url.format(parts);
    next();
  }
}
