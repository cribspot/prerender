var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;

var mongoUri = process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    'mongodb://localhost/prerender';

var database;

MongoClient.connect(mongoUri, function(err, db) {
    database = db;
});

var insertLogInDB = function(obj) {
  database.collection("crawls", function(err, collection) {
    collection.insert(obj);
  });
};

module.exports = {
    beforeSend: function(req, res, next) {
      if (req.prerender.statusCode !== 401) {
        var responseTime = new Date().getTime() - req.prerender.start.getTime();
        insertLogInDB({
          response_time: responseTime,
          page: req.prerender.url,
          code: req.prerender.statusCode,
          user_agent: req.headers["user-agent"],
          date: req.prerender.start
        });
      }
      next();
    }
};
