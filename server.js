#!/usr/bin/env node
var prerender = require('./lib');

var options = {
  ALLOWED_DOMAINS: "www.cribspot.com",
  QUERY_PARAMS: "n,e,s,w",
  BASIC_AUTH_USERNAME: "username", // Set username
  BASIC_AUTH_PASSWORD: "password"  // Set password
};

// Set the options
for (var attrname in options) { process.env[attrname] = options[attrname]; }

var server = prerender({
    workers: process.env.PHANTOM_CLUSTER_NUM_WORKERS,
    iterations: process.env.PHANTOM_WORKER_ITERATIONS || 10,
    phantomBasePort: process.env.PHANTOM_CLUSTER_BASE_PORT || 12300,
    messageTimeout: process.env.PHANTOM_CLUSTER_MESSAGE_TIMEOUT
});


server.use(prerender.basicAuth());
server.use(prerender.whitelist());
server.use(prerender.blacklist());
server.use(prerender.removeQueryParams());
// server.use(prerender.logger());
server.use(prerender.removeScriptTags());
server.use(prerender.httpHeaders());
server.use(require('prerender-mongodb-cache'));
server.use(prerender.logRequest());

server.start();
